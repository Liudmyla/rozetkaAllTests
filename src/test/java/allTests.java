import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.rozetkaMain;

public class allTests extends baseTest {

    Logger logger = LoggerFactory.getLogger(baseTest.class);

    @Test
    public void homeGoodsTest() {
        System.out.println("TEST: Tovary dlia doma");
        rozetkaMain mainpage = new rozetkaMain(driver);

        //tovary dlia doma
        mainpage.homeGoodsOpen();
        System.out.println("Web element found: tovary dlia doma");

        //click element "Vse kategorii" in "tovary dlia doma" section
        mainpage.allCategoriesOpen();
        System.out.println("Web element found: all categories");
    }

    @Test
    public void smartphoneTest() {
        System.out.println("TEST: Smartphone");
        rozetkaMain mainpage = new rozetkaMain(driver);

        //click element "Smartphones, TV, electronics"
        mainpage.smartphonesOpen();
        System.out.println("Web element found: Smartphones, TV, electronics");

        //click element Smartphones in ""Smartphones, TV.." section
        mainpage.smartphonesAllOpen();
        System.out.println("Web element found: Smartphones");

        //write smartphoneList to console and file
        mainpage.findSmartphoneList();
    }

    @Test
    public void washPowders() {
        rozetkaMain mainpage = new rozetkaMain(driver);
        //tovary dlia doma
        mainpage.homeGoodsOpen();
        logger.info("opened: Tovary dlia doma");
        //bytovaia himia
        mainpage.chemicalsOpen();
        logger.info("opened: Chemicals");
        //sredstva dlia stirki
        mainpage.forLaunderOpen();
        logger.info("opened: For launder");
        //stir poroshki
        mainpage.washPowderOpen();
        logger.info("opened: Wash powder");
        //copy list to file
        mainpage.findWashPowderList();
        logger.info("Saved to file: List of wash powder");
    }
}
