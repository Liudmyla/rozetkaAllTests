package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class rozetkaMain extends basePage {

    //Tovary Dlia Doma link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/tovary-dlya-doma/c2394287/']")
    private WebElement homeGoods;

    //Bytova himia link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/bytovaya-himiya/c4429255/']")
    private WebElement chemicals;

    //Bytova himia title
    @FindBy(xpath = "//*[@class='pab-h1']")
    private WebElement chemicalsTitle;

    //Sredstva dlia stirki link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/sredstva-dlya-stirki/c4625084/']")
    private WebElement forLaunder;

    //Stiralnye poroshki link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/sredstva-dlya-stirki/c4625084/filter/tip-sredstva-71899=301219/']")
    private WebElement washPowder;

    //Stiralnye poroshki title
    @FindBy(xpath = "//h1[@itemprop = 'name']")
    private WebElement washPowderTitle;

    //List of wash powder names and prices
    @FindBy(xpath = "//*[@class='g-i-tile-i-title clearfix']/a | //div[@class='g-price-uah']")
    private List<WebElement> washPowderList;

    //Tovary dlia doma: Vsi kategorii link
    @FindBy(xpath = "//*[@class='f-menu-second clearfix']//*[@class='f-menu-cols']//*[@class='f-menu-cols-b clearfix']//*[@href='http://rozetka.com.ua/tovary-dlya-doma/c2394287/']")
    private WebElement homeGoodsAll;

    //Tovary dlia doma: Vsi kategorii page
    @FindBy(xpath = "//*[@class='pab-h1']")
    private WebElement homeGoodsNewPage;

    //smartphones link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/telefony-tv-i-ehlektronika/c4627949/']")
    private WebElement smartphones;

    //All smartphones link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/mobile-phones/c80003/filter/preset=smartfon/']")
    private WebElement smartphonesAll;

    //All smartphones page
    @FindBy(xpath = "//*[normalize-space(@class)='title-page-with-filters-wrap']//*[@itemprop='name']")
    private WebElement smartphonesNewPage;

    //List of smartphones names
    @FindBy(xpath = "//*[@class='g-i-tile-i-box-desc']//*[@class='g-i-tile-i-title clearfix']")
    private List<WebElement> smartphoneList;

    public rozetkaMain(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void homeGoodsOpen() {
        $(homeGoods).click();
    }

    public void chemicalsOpen() {
        $(chemicals).click();
        Assert.assertEquals("Incorrect Chemicals page opened", $(chemicalsTitle).getText(), "Бытовая химия");
    }

    public void forLaunderOpen() {
        $(forLaunder).click();
    }

    public void washPowderOpen() {
        $(washPowder).click();
        Assert.assertEquals("Incorrect WashPowder page opened", $(washPowderTitle).getText(), "Стиральные порошки");
    }

    public void findWashPowderList() {
        basePage.findList("washPowderList.txt", washPowderList);
    }

    public void allCategoriesOpen() {
        $(homeGoodsAll).click();
        Assert.assertEquals("Wrong Tovary_Dlia_Doma page opened", $(homeGoodsNewPage).getText(), "Товары для дома");
    }

    public void smartphonesOpen() {
        $(smartphones).click();
    }

    public void smartphonesAllOpen() {
        $(smartphonesAll).click();
        Assert.assertEquals("Wrong Smartphones page opened", $(smartphonesNewPage).getText(), "Все смартфоны");
    }

    public void findSmartphoneList() {
        basePage.findList("smartphoneList.txt", smartphoneList);
    }

}
