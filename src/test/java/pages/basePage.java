package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class basePage {

    WebDriver driver;

    public basePage(WebDriver driver) {
        this.driver = driver;
    }

    public static void findList(String filename, List<WebElement> elementlist) {
        Formatter listFile = null;
        try {
            listFile = new Formatter(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<SelenideElement> rows = $$(elementlist);
        Iterator<SelenideElement> iter = rows.iterator();
        while (iter.hasNext()) {
            SelenideElement item = iter.next();
            String label = item.getText();
            System.out.println(label);
            listFile.format("%s\n", label);
        }
        listFile.close();
    }
}